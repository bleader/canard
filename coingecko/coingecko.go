package coingecko

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/go-chat-bot/bot"
	"github.com/olekukonko/tablewriter"
	gecko "github.com/superoo7/go-gecko/v3"
	geckotypes "github.com/superoo7/go-gecko/v3/types"
)

var cg *gecko.Client
var list *geckotypes.CoinList
var m sync.Mutex // lock for list access
var mappings = make(map[string][]string)
var defaultChannel string
var dateFmts = []string{
	"2006-01-02",
	"2006/01/02",
	"02-01-2006",
	"02/01/2006",
}
var TZ *time.Location

const mappingsFile = "mappings.json"

const (
	cReset  = "\003"
	cWhite  = "\00300"
	cBlue   = "\00312"
	cGreen  = "\00309"
	cRed    = "\00304"
	cYellow = "\00308"
	cCyan   = "\00311"
	cPink   = "\00313"
)

const thresholdDefault = 1.0

func parseDate(dateStr string) (date time.Time, err error) {
	for _, fmt := range dateFmts {
		date, err = time.ParseInLocation(fmt, dateStr, TZ)
		if err == nil {
			break
		}
	}

	return
}

func colorPercent(v float64) string {
	var colorCode string

	if v > 0 {
		colorCode = cGreen
	} else {
		colorCode = cRed
	}

	return fmt.Sprintf("%s%.3f%s", colorCode, v, cReset)
}

func colorFiat(fiat string) string {
	var colorCode string
	var sym string

	if fiat == "usd" {
		colorCode = cCyan
		sym = "$"
	} else if fiat == "eur" {
		colorCode = cYellow
		sym = "€"
	} else {
		colorCode = cPink
		sym = strings.ToUpper(fiat)
	}

	return fmt.Sprintf("%s%s%s", colorCode, sym, cReset)
}

func relativeTime(dateStr string) (relTimeStr string, err error) {
	ATHDate, err := time.Parse(time.RFC3339, dateStr)
	if err != nil {
		return
	}

	elapsed := time.Since(ATHDate)
	hours := elapsed.Hours()
	mins := elapsed.Minutes()
	secs := elapsed.Seconds()

	if hours > 48 {
		relTimeStr = ATHDate.Format("2006-01-02")
	} else if hours > 1 {
		relTimeStr = fmt.Sprintf("%d hours ago", int(hours))
	} else if mins > 1 {
		relTimeStr = fmt.Sprintf("%d minutes ago", int(mins))
	} else if secs > 1 {
		relTimeStr = fmt.Sprintf("%d seconds ago", int(secs))
	} else {
		relTimeStr = "just now"
	}

	return
}

func expandListArgs(args []string) (parsed []string) {
	/* expand comma separated values */
	for _, c := range args {
		if strings.ContainsRune(c, ',') {
			tmp := strings.Split(c, ",")
			parsed = append(parsed, tmp...)
		} else {
			parsed = append(parsed, c)
		}
	}

	return
}

func getCoinsIDList(args []string) (IDs []string, notFound []string) {
	input := expandListArgs(args)

	/* Build the list of IDs */
	for _, c := range input {
		found := false

		/* check for mapping */
		if val, ok := mappings[c]; ok {
			IDs = append(IDs, val...)
			continue
		}

		/* find matching IDs */
		m.Lock()
		for _, lv := range *list {

			if strings.EqualFold(c, lv.ID) ||
				strings.EqualFold(c, lv.Name) ||
				strings.EqualFold(c, lv.Symbol) {
				IDs = append(IDs, lv.ID)
				found = true
				if strings.EqualFold(c, lv.ID) {
					break
				}
			}
		}
		m.Unlock()

		if !found {
			notFound = append(notFound, c)
		}
	}

	sort.Strings(IDs)
	sort.Strings(notFound)
	return
}

func getValues(coins []string, fiat string) (msg string, err error) {
	var data [][]string
	var coinsErr []string
	coins, notFound := getCoinsIDList(coins)
	coloredFiat := colorFiat(fiat)

	for _, c := range coins {
		/* get info */
		coin, err := cg.CoinsID(c, false, false, true, false, false, false)
		if err != nil {
			coinsErr = append(coinsErr, c)
			continue
		}

		/* build info line for coin */
		hour := coin.MarketData.PriceChangePercentage1hInCurrency[fiat]
		day := coin.MarketData.PriceChangePercentage24hInCurrency[fiat]
		week := coin.MarketData.PriceChangePercentage7dInCurrency[fiat]
		price := coin.MarketData.CurrentPrice[fiat]
		ATH := coin.MarketData.ATH[fiat]
		ATHPercent := coin.MarketData.ATHChangePercentage[fiat]
		ATHRelTimeStr, err := relativeTime(coin.MarketData.ATHDate[fiat])
		if err != nil {
			return "", err
		}

		data = append(data, []string{
			fmt.Sprintf("%s%s%s", cWhite, coin.Name, cReset),
			strings.ToUpper(coin.Symbol),
			fmt.Sprintf("%s%s", strconv.FormatFloat(price, 'f', -1, 64), coloredFiat),
			fmt.Sprintf("1h: %s", colorPercent(hour)),
			fmt.Sprintf("24h: %s", colorPercent(day)),
			fmt.Sprintf("7d: %s", colorPercent(week)),
			fmt.Sprintf("ATH: %s%s", strconv.FormatFloat(ATH, 'f', -1, 64), coloredFiat),
			fmt.Sprintf("%.3f", ATHPercent),
			ATHRelTimeStr,
		})

		/* add to tracked coins */
		trackCoin(c)
	}

	if len(data) > 0 {
		buf := new(bytes.Buffer)
		table := tablewriter.NewWriter(buf)
		table.SetBorders(tablewriter.Border{Left: false, Top: false, Right: false, Bottom: false})

		for _, v := range data {
			table.Append(v)
		}

		table.Render()
		msg = buf.String()
	}

	if len(notFound) > 0 {
		msg += fmt.Sprintf("Symbol(s) not found: %v", notFound)
	}
	if len(coinsErr) > 0 {
		msg += fmt.Sprintf("Error on query: %v", coinsErr)
	}

	return
}

func cmdQueryAny(cmd *bot.Cmd) (msg string, err error) {
	if len(cmd.Args) < 2 {
		return "", errors.New("execting at least 2 arguments")
	}

	return getValues(cmd.Args[1:], cmd.Args[0])
}

func cmdQueryUSD(cmd *bot.Cmd) (msg string, err error) {
	if len(cmd.Args) < 1 {
		return "", errors.New("execting at least 1 argument")
	}

	return getValues(cmd.Args, "usd")
}

func cmdQueryEUR(cmd *bot.Cmd) (msg string, err error) {
	if len(cmd.Args) < 1 {
		return "", errors.New("execting at least 1 argument")
	}

	return getValues(cmd.Args, "eur")
}

func cmdPriceEvolution(cmd *bot.Cmd) (msg string, err error) {
	var fromDate time.Time
	var toDate time.Time

	if len(cmd.Args) < 2 {
		return "", errors.New("execting at least 2 argument")
	}

	IDs, _ := getCoinsIDList([]string{cmd.Args[0]})
	if len(IDs) < 1 {
		return "", errors.New(fmt.Sprintf("%s not found", cmd.Args[0]))
	}

	if len(IDs) > 1 {
		msg = fmt.Sprintf("found %v, using %s\n", IDs, IDs[0])
	}
	ID := IDs[0]

	if len(cmd.Args) == 2 {
		toDate = time.Now()
	} else {
		toDate, err = parseDate(cmd.Args[2])
		if err != nil {
			return
		}
	}

	fromDate, err = parseDate(cmd.Args[1])
	if err != nil {
		return
	}

	fromRes, err := cg.CoinsIDHistory(ID, fromDate.Format("02-01-2006"), false)
	if err != nil {
		return
	}

	toRes, err := cg.CoinsIDHistory(ID, toDate.Format("02-01-2006"), false)
	if err != nil {
		return
	}

	if (*fromRes).MarketData == nil {
		return "", errors.New(fmt.Sprintf("no data found for %s @ %s", ID,
			fromDate.Format("2006-01-02")))
	}
	if (*toRes).MarketData == nil {
		return "", errors.New(fmt.Sprintf("no data found for %s @ %s", ID,
			toDate.Format("2006-01-02")))
	}

	fromPrice := (*fromRes).MarketData.CurrentPrice["usd"]
	toPrice := (*toRes).MarketData.CurrentPrice["usd"]

	diff := (((toPrice / fromPrice) - 1.0) * 100)

	msg += fmt.Sprintf("%s: (%s) %f%s ==> %f%s (%s) [%s%%]",
		ID,
		fromDate.Format("2006-01-02"), fromPrice, colorFiat("usd"),
		toPrice, colorFiat("usd"), toDate.Format("2006-01-02"),
		colorPercent(diff))

	return
}

func saveMapping() error {
	str, err := json.Marshal(mappings)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(mappingsFile, str, 0600)
}

func cmdMap(cmd *bot.Cmd) (msg string, err error) {
	if len(cmd.Args) == 0 {
		return mapList(cmd)
	}

	if len(cmd.Args) == 1 {
		if mapping, ok := mappings[cmd.Args[0]]; ok {
			msg = fmt.Sprintf("%s -> %v", cmd.Args[0], mapping)
		} else {
			msg = fmt.Sprintf("%s: no such mapping", cmd.Args[0])
		}

		return
	}

	if len(cmd.Args) < 2 {
		return "", errors.New("expecting at least 2 arguments")
	}

	IDs := expandListArgs(cmd.Args[1:])
	name := cmd.Args[0]

	for _, ID := range IDs {
		found := false

		m.Lock()
		for _, lv := range *list {
			if ID == lv.ID {
				found = true
				continue
			}
		}
		m.Unlock()
		if !found {
			return fmt.Sprintf("%s is not a valid ID", ID), nil
		}
	}

	mappings[name] = IDs
	err = saveMapping()
	if err != nil {
		return "", err
	}
	return "Mapping added successfully", nil
}

func cmdMapRm(cmd *bot.Cmd) (msg string, err error) {
	if len(cmd.Args) != 1 {
		return "", errors.New("expecting 1 argument")
	}

	if _, ok := mappings[cmd.Args[0]]; !ok {
		return "No such mapping", nil
	}

	delete(mappings, cmd.Args[0])
	err = saveMapping()
	if err != nil {
		return "", err
	}
	return "Mapping removed successfully", nil
}

func mapList(_ *bot.Cmd) (msg string, err error) {
	if len(mappings) == 0 {
		return "No mapping found", nil
	}

	for k, v := range mappings {
		msg += fmt.Sprintf("%s => %v\n", k, v)
	}

	return
}

func periodicCmdListUpdateDaily() (ret []bot.CmdResult, err error) {
	msg, err := cmdListUpdate(nil)
	if err != nil {
		fmt.Println(err)
	}
	ret = append(ret, bot.CmdResult{
		Message: msg,
		Channel: defaultChannel,
	})
	return
}

func cmdListUpdate(_ *bot.Cmd) (msg string, err error) {
	before := 0
	m.Lock()
	if list != nil {
		before = len(*list)
	}

	list, err = cg.CoinsList()
	if err != nil {
		m.Unlock()
		return "", errors.New("unable to update coins list")
	}
	after := len(*list)
	m.Unlock()
	msg = fmt.Sprintf("Updated coins list (from %d to %d coins)", before, after)

	return
}

func cmdFindCoins(cmd *bot.Cmd) (msg string, err error) {
	var notFound []string

	if len(cmd.Args) < 1 {
		return "", errors.New("expecting at least 1 argument")
	}

	coins := expandListArgs(cmd.Args)

	for _, c := range coins {
		found := false
		m.Lock()
		for _, lv := range *list {
			if strings.EqualFold(c, lv.ID) ||
				strings.EqualFold(c, lv.Name) ||
				strings.EqualFold(c, lv.Symbol) {
				found = true
				msg += fmt.Sprintf("%s => [ID: %s] [Name: %s] [Symbol: %s]\n",
					c, lv.ID, lv.Name, lv.Symbol)
			}
		}
		m.Unlock()
		if !found {
			notFound = append(notFound, c)
		}
	}

	if len(notFound) > 0 {
		msg += fmt.Sprintf("Symbol(s) not found: %v", notFound)
	}

	return
}

func cmdConvert(cmd *bot.Cmd) (msg string, err error) {
	if len(cmd.Args) != 3 {
		return "", errors.New("expected 3 arguments")
	}

	amount, err := strconv.ParseFloat(cmd.Args[0], 64)
	if err != nil {
		return "", errors.New("unable to parse amount")
	}

	from, _ := getCoinsIDList([]string{cmd.Args[1]})
	if len(from) > 1 {
		return fmt.Sprintf("%s matches more than one coin: %v", cmd.Args[1], from), nil
	} else if len(from) == 0 {
		return fmt.Sprintf("%s doesn't match any coin", cmd.Args[1]), nil
	}

	to, _ := getCoinsIDList([]string{cmd.Args[2]})
	if len(to) > 1 {
		return fmt.Sprintf("%s matches more than one coin: %v", cmd.Args[2], from), nil
	} else if len(to) == 0 {
		return fmt.Sprintf("%s doesn't match any coin", cmd.Args[2]), nil
	}

	fromID := from[0]
	toID := to[0]
	IDs := []string{fromID, toID}
	vc := []string{"usd"}
	sp, err := cg.SimplePrice(IDs, vc)
	if err != nil {
		return "", err
	}

	fromPrice := (*sp)[fromID]["usd"]
	toPrice := (*sp)[toID]["usd"]

	msg = fmt.Sprintf("%f %s ~= %f %s", amount, fromID,
		float64(fromPrice/toPrice)*amount, toID)
	return
}

// TODO remove once running for a while with new commands
func oldUnmap(_ *bot.Cmd) (msg string, err error) {
	msg = "!unmap replaced by !maprm"
	return
}

// TODO remove once running for a while with new commands
func oldMapList(_ *bot.Cmd) (msg string, err error) {
	msg = "!maplist replaced by !map without parameter"
	return
}

func init() {
	mapStr, err := ioutil.ReadFile(mappingsFile)
	if err != nil {
		fmt.Println("could not read mappings file")
	} else {
		mappings = make(map[string][]string)
		json.Unmarshal(mapStr, &mappings)
	}

	channel := os.Getenv("IRC_CHAN")
	if len(channel) == 0 {
		defaultChannel = "console"
	} else {
		defaultChannel = channel
	}

	thresholdEnv := os.Getenv("TRACK_THRESH")
	if len(thresholdEnv) == 0 {
		threshold = thresholdDefault
	} else {
		thr, err := strconv.ParseFloat(thresholdEnv, 32)
		if err != nil {
			fmt.Println("unable to parse threshold:", err)
			threshold = thresholdDefault
		} else {
			threshold = float32(thr)
		}
	}

	httpClient := &http.Client{
		Timeout: time.Second * 30,
	}
	cg = gecko.NewClient(httpClient)

	TZ, err = time.LoadLocation("Europe/Paris")
	if err != nil {
		fmt.Println("could not load timezone")
	}

	// price queries
	bot.RegisterCommand(
		"a",
		"query coingecko for coins value in <fiat>",
		"<fiat> btc [eth] [...]",
		cmdQueryAny)

	bot.RegisterCommand(
		"u",
		"query coingecko for coin value in USD",
		"btc [eth] [...]",
		cmdQueryUSD)

	bot.RegisterCommand(
		"e",
		"query coingecko for coin value in EUR",
		"btc [eth] [...]",
		cmdQueryEUR)

	bot.RegisterCommand(
		"evol",
		"get price evolution between two dates",
		"btc <since> [<upto>]",
		cmdPriceEvolution)

	// mappings
	bot.RegisterCommand(
		"map",
		"map short name to a list of coin IDs (see !find)",
		"<name> coinID1 [coinID2] [...]",
		cmdMap)

	bot.RegisterCommand(
		"maprm",
		"unmap short name to coin IDs",
		"abs",
		cmdMapRm)

	bot.RegisterCommand(
		"update",
		"update  current coins list",
		"",
		cmdListUpdate)

	bot.RegisterCommand(
		"find",
		"find coins and get ID, Name & Symbol",
		"abs [rbc] [...]",
		cmdFindCoins)

	bot.RegisterCommand(
		"convert",
		"convert X coins1 to coins2",
		"<amount> coinID1 coinID2",
		cmdConvert)

	bot.RegisterPeriodicCommandV2(
		"periodicListUpdate",
		bot.PeriodicConfig{
			CronSpec:  "0 0 4 * * *", // everyday at 4am
			CmdFuncV2: periodicCmdListUpdateDaily,
		})

	// tracking commands
	bot.RegisterCommand(
		"track",
		"list tracked coins",
		"",
		cmdTrackedList)

	bot.RegisterCommand(
		"trackrm",
		"remove coin(s) from tracking",
		"bitcoin [...]",
		cmdTrackRemove)

	bot.RegisterCommand(
		"trackthreshold",
		"query or set threshold percentage / minute for tracking alert",
		"2.5",
		cmdTrackThreshold)

	bot.RegisterPeriodicCommandV2(
		"periodicTrackedCheck",
		bot.PeriodicConfig{
			CronSpec:  "@every 10m",
			CmdFuncV2: periodicCmdTrackedCheck,
		})

	// TODO remove once running for a while with new commands
	// deprecated warnings
	bot.RegisterCommand(
		"unmap",
		"replaced by !maprm",
		"",
		oldUnmap)

	bot.RegisterCommand(
		"maplist",
		"replaced by !map without parameter",
		"",
		oldMapList)

	_, err = cmdListUpdate(nil)
	if err != nil {
		log.Fatal(err)
	}

}
