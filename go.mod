module ratonland.org/canard

go 1.15

require (
	github.com/go-chat-bot/bot v0.0.0-20201004141219-763f9eeac7d5
	github.com/go-chat-bot/plugins v0.0.0-20201024114236-00ff43fcf77f
	github.com/mattn/go-shellwords v1.0.11 // indirect
	github.com/robfig/cron/v3 v3.0.1 // indirect
	github.com/superoo7/go-gecko v1.0.0 // indirect
	ratonland.org/coingecko v0.0.0
	ratonland.org/reminder v0.0.0
	ratonland.org/version v0.0.0-00010101000000-000000000000
)

replace ratonland.org/coingecko => ./coingecko

replace ratonland.org/reminder => ./reminder

replace ratonland.org/version => ./version
