package version

import (
	"fmt"

	"github.com/go-chat-bot/bot"
)

var BotVersion string

func cmdVersion(_ *bot.Cmd) (msg string, err error) {
	return fmt.Sprintf("canard %s ( https://gitlab.com/bleader/canard/-/blob/master/CHANGELOG.md )", BotVersion), nil
}

func init() {
	bot.RegisterCommand(
		"version",
		"display bot version",
		"",
		cmdVersion)
}
