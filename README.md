# Canard

Canard is an irc bot meant to allow getting information about crypto currencies
directly in a given channel.

## Features

- Query [coingecko](http://coingecko.com) for price of coins, see [Coin
  Queries](#coin-queries).
- Tracking queried coins for big movements, see [Tracking](#tracking).
- Settings reminders, thought for airdrops but works for anything, see
  [Reminder](#reminder).
- [IRC Client](#irc) and a [debug console mode](#debug)

## IRC

The IRC client of the bot can be configured using ENV variables.

- `IRC_SERV`: irc server to connect to
- `IRC_CHAN`: channel to join upon connection
- `IRC_TLS`: connect using TLS
- `IRC_NICK`: bot nick and user name
- `IRC_PASS`: password sent on connection (via /quote PASS)
- `IRC_DBG`: enable debug output on console

`IRC_TLS` and `IRC_DBG` are boolean and can be set using `1` on `true`.

## Commands

### Coingecko

Commands allowing list of coins can be done with spaces or comma, and they can
be mixed so all these are valid:
- `!u btc eth`
- `!u btc,eth`
- `!u btc abs,rbc eth`
- `!u cake,reef bunny,auto`

This kind of list is referenced as `<cointlist>` further in this document.

#### Coin Queries

- `!u <coinlist>`: check price of coins in `<coinlist>` in USD
- `!e <coinlist>`: check price of coins in `<coinlist>` in EUR
- `!a <fiat> <coinlist>`: check price of coins in `<coinlist>` in `<fiat>`

*NOTE*: `<fiat>` can actually be another coin.

#### Mappings

- `!map`: list registered mappings
- `!map name`: show mapping matching name
- `!map name coinID1 [coinID2] [...]`: allow `name` to be used instead of a
  coin name of symbol in queries
- `!maprm name`: remove the mapping done on `name`

#### Conversion

- `!convert amount coinID1 coinID2`: convert the `<amount>` of `coinID1` to
  `coinID2`. Will warn if there is more than one ID matching what was provided,
  but respects mappings.

**WARNING**: This conversion get prices in USD on coingecko and make a
mathematical conversion, it is not related to the price of the pair on any
exchange, and could be off.

#### Evolution

- `!evol <coin> <fromDate> [<toDate>]`: report the evolution of a coin between
  two dates, if second one is omitted, using today. Coingecko API only support
  date, no time.

#### Misc

- `!update`: updates the coinlist from coingecko in cases it became oboslete
- `!find <coinlist>`: shows the ID, Name, and Symbol of matching coins, useful
  to find IDs in order to create mappings

#### Tracking

Queried coins will be added to a tracking list, coins not queried for 4 days
will be removed. These coins will be checked every 10 minutes, and if price
movement is bigger than `<threshold>`/minute, an alert will be sent to the
default channel.

`<threshold>` is set to 1%/minute by default, to override it, you can use
the environment variable `TRACK_THRESH` but it can also be changed at runtime
(this is not saved on restart).

- `!track`: list tracked coins, their last query time, and last price update
- `!trackthreshold  <threshold>`: set percent move over last 10 minutes to
  trigger an alert

### Reminder

You can add reminders for a given date with an arbitrary message. This works in
a channel and in query, reminders are per _target_: a channel, or a nick and
cannot be seen or removed from another target. Once triggered, reminders are
forever deleted.

- `!rem`: list active reminders with their `<date>` and `<ID>`
- `!rem <date> message`: add a reminder on `<date>`, return an `<ID>`
- `!remrm <ID>`: remove the reminder `<ID>`

Date & Time Format:
Dates & Times can be entered in various formats, if time is omitted it will
take the specified day from the date and put it an 11:00. Date and Time can be
separated by a space or by `T`.

Time:
- `HH:MM`
- `HHhMM`

Date:

- `YYYY-MM-DD`
- `YYYY/MM/DD`
- `DD-MM-YYYY`
- `DD/MM/YYYY`

A few examples:

- `!rem 2021-04-12 test 1`: will set reminder for 12th of april 2021 at 11:00
- `!rem 13/03/2022 23:00 test 2`: will set reminder for 13th of march 2022 at 23:00
- `!rem 2021-04-14 14h00 test 3`: will set reminder for 14th of april 2021 at 14:00

Points to keep in mind:
- reminders are stored in clear
- the bot does not track nick changes, so if you change nick:
    - you won't receive notifications made from your previous nick
    - anybody can take your nick and query the bot for your previous reminders
- IDs are global, but you cannot remove notifications created in query from a
  channel or from another nick

## Debug

In order to test without connecting to irc, you can use the debug console mode of
`go-chat-bot`.

```BOT_CONSOLE=1 ./canard```
