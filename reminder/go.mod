module ratonland.org/reminder

go 1.15

require (
	github.com/araddon/dateparse v0.0.0-20210207001429-0eec95c9db7e
	github.com/go-chat-bot/bot v0.0.0-20201004141219-763f9eeac7d5
	github.com/superoo7/go-gecko v1.0.0
)
