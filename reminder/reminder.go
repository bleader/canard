package reminder

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math/rand"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/go-chat-bot/bot"
)

type Reminder struct {
	Channel string
	Date    time.Time
	Text    string
}

const remindersFile = "reminders.json"
const MaxUint16 = ^uint16(0)

var dateFmts = []string{
	"2006-01-02 15:04",
	"2006-01-02 15h04",
	"2006-01-02T15:04",
	"2006-01-02T15h04",
	"2006/01/02 15:04",
	"2006/01/02 15h04",
	"2006/01/02T15:04",
	"2006/01/02T15h04",
	"02-01-2006 15:04",
	"02-01-2006 15h04",
	"02-01-2006T15:04",
	"02-01-2006T15h04",
	"02/01/2006 15:04",
	"02/01/2006 15h04",
	"02/01/2006T15:04",
	"02/01/2006T15h04",
	"2006-01-02",
	"2006/01/02",
	"02-01-2006",
	"02/01/2006",
}

var TZ *time.Location

var reminders map[uint16]Reminder
var m sync.Mutex // to lock accesses to map and file

func targetFromCmd(cmd *bot.Cmd) string {
	target := cmd.Channel
	if target[0] != '#' {
		target = cmd.User.Nick
	}

	return target
}

func saveReminders() error {
	m.Lock()

	str, err := json.Marshal(reminders)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(remindersFile, str, 0600)

	m.Unlock()
	return err
}

func parseDate(dateStr string) (date time.Time, err error) {
	for _, fmt := range dateFmts {
		date, err = time.ParseInLocation(fmt, dateStr, TZ)
		if err == nil {
			break
		}
	}

	if date.Hour() == 0 && date.Minute() == 0 {
		date = date.Add(11 * time.Hour)
	}

	return
}

func cmdReminder(cmd *bot.Cmd) (msg string, err error) {
	if len(cmd.Args) == 0 {
		return remindersList(cmd)
	}

	if len(cmd.Args) < 2 {
		return "", errors.New("expecting at least 2 arguments")
	}

	remIDX := 2
	date, err := parseDate(fmt.Sprintf("%s %s", cmd.Args[0], cmd.Args[1]))
	if err != nil {
		date, err = parseDate(fmt.Sprintf(cmd.Args[0]))
		if err != nil {
			return "", errors.New("cannot parse date")
		}

		remIDX = 1
	}

	if len(cmd.Args[remIDX:]) < 1 {
		return "", errors.New("missing reminder text")
	}

	/* reminder */
	var ID uint16
	remStr := strings.Join(cmd.Args[remIDX:], " ")

	m.Lock()
	for ID = uint16(rand.Intn(int(MaxUint16))); ; {
		if _, ok := reminders[ID]; !ok {
			break
		}
	}

	target := targetFromCmd(cmd)
	reminders[ID] = Reminder{
		Channel: target,
		Date:    date,
		Text:    remStr,
	}
	m.Unlock()

	err = saveReminders()
	if err != nil {
		return
	}

	msg = fmt.Sprintf("Added reminder [%04x] on %s: %s",
		ID, date.Format("2006-01-02 at 15:04"), remStr)
	return
}

func cmdReminderRm(cmd *bot.Cmd) (msg string, err error) {
	if len(cmd.Args) < 1 {
		return "", errors.New("expecting at least 1 argument")
	}

	target := targetFromCmd(cmd)
	var notFound []string
	m.Lock()
	for _, IDStr := range cmd.Args {

		ID64, err := strconv.ParseUint(IDStr, 16, 16)
		ID := uint16(ID64)
		if err != nil {
			msg += "Unable to parse\n"
			continue
		}
		if _, ok := reminders[ID]; !ok {
			notFound = append(notFound, IDStr)
			continue
		}

		if target != reminders[ID].Channel {
			notFound = append(notFound, IDStr)
			continue
		}

		msg = fmt.Sprintf("Removed: [%04x] on %s: %s",
			ID,
			reminders[ID].Date.Format("2006-01-02 at 15:04"),
			reminders[ID].Text)
		delete(reminders, ID)
	}
	m.Unlock()

	if len(notFound) > 0 {
		msg += fmt.Sprintf("Not found: %v\n", notFound)
	}

	err = saveReminders()
	if err != nil {
		return
	}
	return
}

func remindersList(cmd *bot.Cmd) (msg string, err error) {
	target := targetFromCmd(cmd)
	m.Lock()

	for ID, rem := range reminders {
		if rem.Channel != target {
			continue
		}
		msg += fmt.Sprintf("[%04x][%s] %s\n",
			ID, rem.Date.Format("2006-01-02 15:04"), rem.Text)
	}
	m.Unlock()

	if len(msg) == 0 {
		msg = "No reminder found"
	}

	return
}

func periodicCmdRemindersChecker() (ret []bot.CmdResult, err error) {
	var msg string
	changed := false
	now := time.Now()
	m.Lock()

	for ID, rem := range reminders {
		if rem.Date.Before(now) {
			if now.Sub(rem.Date).Hours() > 1 {
				msg = fmt.Sprintf("[OVERDUE REMINDER] (%s) %s",
					rem.Date.Format("2006-01-02 15:04"), rem.Text)
			} else {
				msg = fmt.Sprintf("[REMINDER] %s", rem.Text)
			}
			ret = append(ret, bot.CmdResult{
				Message: msg,
				Channel: rem.Channel,
			})
			delete(reminders, ID)
			changed = true
		}
	}
	m.Unlock()

	if changed {
		err = saveReminders()
		if err != nil {
			return
		}
	}

	return
}

// TODO remove once running for a while with new commands
func oldListReminders(_ *bot.Cmd) (msg string, err error) {
	msg = "!remlist replaced by !rem with no arguments"
	return
}

func init() {
	m.Lock()
	mapStr, err := ioutil.ReadFile(remindersFile)
	if err != nil {
		fmt.Println("could not read reminders file")
		reminders = make(map[uint16]Reminder)
	} else {
		json.Unmarshal(mapStr, &reminders)
	}
	m.Unlock()

	rand.Seed(time.Now().UnixNano())

	TZ, err = time.LoadLocation("Europe/Paris")
	if err != nil {
		fmt.Println("could not load timezone")
	}

	bot.RegisterCommand(
		"rem",
		"add a reminder",
		"<date> <reminder string>",
		cmdReminder)

	bot.RegisterCommand(
		"remrm",
		"remove reminder <ID>",
		"<ID>",
		cmdReminderRm)

	// TODO remove once running for a while with new commands
	// deprecated warnings
	bot.RegisterCommand(
		"remlist",
		"replaced by !rem without parameter",
		"",
		oldListReminders)

	bot.RegisterPeriodicCommandV2(
		"periodicRemindersCheck",
		bot.PeriodicConfig{
			CronSpec:  "@every 1m",
			CmdFuncV2: periodicCmdRemindersChecker,
		})
}
