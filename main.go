package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	_ "ratonland.org/coingecko"
	_ "ratonland.org/reminder"
	_ "ratonland.org/version"

	"github.com/go-chat-bot/bot"
	"github.com/go-chat-bot/bot/irc"
)

func main() {
	console := os.Getenv("BOT_CONSOLE")
	if len(console) > 0 {
		ConsoleMain()
	} else {
		IRCMain()
	}
}

func IRCMain() {
	ircTLS := false
	ircDebug := false

	ircServer := os.Getenv("IRC_SRV")
	if len(ircServer) == 0 {
		ircServer = "chat.freenode.net:6697"
	}

	ircChannel := os.Getenv("IRC_CHAN")
	if len(ircChannel) == 0 {
		ircChannel = "#ratontest"
	}

	ircNick := os.Getenv("IRC_NICK")
	if len(ircNick) == 0 {
		ircNick = "canard"
	}

	ircPass := os.Getenv("IRC_PASS")

	ircEnvTLS := os.Getenv("IRC_TLS")
	if ircEnvTLS == "1" || strings.EqualFold(ircEnvTLS, "true") {
		ircTLS = true
	}

	ircEnvDebug := os.Getenv("IRC_DBG")
	if ircEnvDebug == "1" || strings.EqualFold(ircEnvDebug, "true") {
		ircDebug = true
	}

	irc.Run(&irc.Config{
		Server:   ircServer,
		Channels: []string{ircChannel},
		User:     ircNick,
		Nick:     ircNick,
		Password: ircPass,
		UseTLS:   ircTLS,
		Debug:    ircDebug})
}

func ConsoleMain() {
	nick := "bot"
	b := bot.New(&bot.Handlers{
		Response: func(target string, message string, sender *bot.User) {
			if message == "" {
				return
			}
			fmt.Println(fmt.Sprintf("%s:\n%s", nick, message))
		},
	},
		&bot.Config{
			Protocol: "debug",
			Server:   "debug",
		},
	)

	fmt.Println("Type a command or !help for available commands...")

	for {
		r := bufio.NewReader(os.Stdin)

		input, err := r.ReadString('\n')
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		b.MessageReceived(
			&bot.ChannelData{
				Protocol:  "debug",
				Server:    "",
				Channel:   "console",
				IsPrivate: true,
			},
			&bot.Message{Text: input},
			&bot.User{ID: "id", RealName: "Debug Console", Nick: nick, IsBot: false})
	}
}
