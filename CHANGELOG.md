# Changelog

## 0.8.1 - 2023-03-19
### Added
- support for IRC_PASS from env
### Fixed
- correct a typo in README.md thanks to @CharlyBr

## 0.8.0 - 2021-05-02
### Added
- !evol command
- !map <mapname>

## 0.7.1 - 2021-04-26
### Changed
- tweaked and fixed colors and spacing

## 0.7.0 - 2021-04-22
### Added
- added version module with !version command
- !convert command
### Changed
- reworked queries output with table and changed colors

## 0.6.1 - 2021-04-18
### Added
- license file
### Fixed
- protect coins list by a mutex
- tracking minutes were cast to int, added a round before that
### Changed
- one error won't throw away all queried coins

## 0.6.0 - 2021-04-12
### Added
- tracking of queried coins for fast movement alerts

## 0.5.2 - 2021-04-11
### Fixed
- remove debug print

## 0.5.1 - 2021-04-11
### Added
- automatic coin list update daily
### Fixed
- segfault when creating mapping with no file yet
- expand parameters in !map

## 0.5.0 - 2021-04-10
### Added
- reminder module
- include console as an option instead cherry-picking
### Fixed
- proper relative time
- console use hardcoded nick to avoid nil sender segfaults

## 0.4.2 - 2021-04-06
### Fixed
- use actual proper reset code for colors

## 0.4.1 - 2021-04-06
### Changed
- removed color for ATH percentage
### Fixed
- use proper reset code for colors

## 0.4.0 - 2021-04-04
### Added
- make irc client configurable through environment variables

## 0.3.1 - 2021-04-04
### Changed
- list of symbol not found if any at the end of !find

## 0.3.0 - 2021-04-03
### Added
- find command
- list of symbol not found if any at the end of report

### Changed
- mapping can now point to a list of IDs
- only IDs are allowed as mapping targets
- list can be passed with a mix of spaces and comma
 
## 0.2.0 - 2021-04-03
### Added
- Color fiat symbols
- ATH in queries output
 
## 0.1.1 - 2021-03-29
### Fixed
- Typo in README
- Increase http client timeout

## 0.1.0 - 2021-03-28
### Added
- First working version

