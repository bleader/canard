VERSION=$(shell git describe --tags)

all: build

build: canard

canard:
	go build -ldflags "-X ratonland.org/version.BotVersion=${VERSION}"

debug: build
	BOT_CONSOLE=1 ./canard

clean:
	rm canard
